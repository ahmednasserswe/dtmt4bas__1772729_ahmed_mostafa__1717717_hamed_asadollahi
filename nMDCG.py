import pandas as pd
import math
import numpy


def buildQueryIdToDocIdDict(dataFrame, queryIdIndex, docIdIndex ):
    queryIDToDocsIdDict = {}
    for row in dataFrame.iterrows():
        queryID = row[1][queryIdIndex]
        docID = row[1][docIdIndex]

        if queryID in queryIDToDocsIdDict:
            docArray = queryIDToDocsIdDict[queryID]
            docArray.append(docID)
        else:
            docArray = []
            docArray.append(docID)
            queryIDToDocsIdDict[queryID] = docArray
    return  queryIDToDocsIdDict


def calculateRelevance(docID, groundTruthDocsArr):
    if docID in groundTruthDocsArr:
        return 1
    else:
        return 0

def calculateMDCG(queryId, k, given_groundTruthDataQToD_Dict, given_resultsDataQToD_Dict):
    resultsDocsArr = given_resultsDataQToD_Dict[queryId]
    groundTruthDocsArr = given_groundTruthDataQToD_Dict[queryId]
    doc1Relevance = calculateRelevance(resultsDocsArr[0], groundTruthDocsArr)
    # print resultsDocsArr[0]

    if k == 1:
        return doc1Relevance
    else:
        sumRelv = 0
        for rank in range(2, k):
            if rank > len(resultsDocsArr):
                break
            sumRelv += (calculateRelevance(resultsDocsArr[rank-1], groundTruthDocsArr) / math.log(rank,2))
        return doc1Relevance + sumRelv


def calculateAverageNMDCG(toTestFile, groundTruthFile, k):
    groundTruthDataFrame = pd.read_csv(groundTruthFile, sep='\t')
    resultsDataFame = pd.read_csv(toTestFile, sep='\t')
    groundTruthDataQToD_Dict = buildQueryIdToDocIdDict(groundTruthDataFrame, 0, 1)
    resultsDataQToD_Dict = buildQueryIdToDocIdDict(resultsDataFame, 0, 1)

    nMDCGArr = []
    for queryID in resultsDataQToD_Dict.keys():
        MDCG = calculateMDCG(queryID, k, groundTruthDataQToD_Dict, resultsDataQToD_Dict)
        MaximumMDCG = calculateMDCG(queryID, k, groundTruthDataQToD_Dict, groundTruthDataQToD_Dict)
        # print queryID
        nMDCG = MDCG / MaximumMDCG
        nMDCGArr.append(nMDCG)

    # print (nMDCGArr)
    # print ()
    return numpy.average(nMDCGArr)

