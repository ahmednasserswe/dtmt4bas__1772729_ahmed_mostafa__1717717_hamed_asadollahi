import pandas as pd
import math




def getTopKDocsId(rankings1DF, rankings2DF, k):
    topKSet = set()
    for i in range(0,max(len(rankings1DF),len(rankings2DF))):
        if i>=len(rankings1DF) or i>=len(rankings2DF) or len(topKSet) == k:
            break
        topKSet.add(rankings1DF.iloc[i]['Doc_ID'])
        topKSet.add(rankings2DF.iloc[i]['Doc_ID'])
    return topKSet

def combineTwoRankings(queryID,rankingsTitleDF,rankingTitle_multiplier, rankingsTextDF, rankingText_multiplier,topKSet):
    newDF = pd.DataFrame( columns=('Query_ID', 'Doc_ID', 'Rank', 'Score'))

    for doc_id in topKSet:
        fromTitle = rankingsTitleDF.loc[rankingsTitleDF['Doc_ID'] == doc_id]
        fromText = rankingsTextDF.loc[rankingsTextDF['Doc_ID'] == doc_id]
        newScore = 0
        fromTitle_score = 0
        fromText_score = 0
        if len(fromTitle) > 0:
            fromTitle_score = fromTitle.iloc[0]['Doc_ID']
        if len(fromText) > 0:
            fromText_score = fromText.iloc[0]['Doc_ID']
        newScore = fromTitle_score*rankingTitle_multiplier + fromText_score*rankingText_multiplier
        newRow = pd.DataFrame([[queryID,doc_id,0,newScore] ],columns=('Query_ID', 'Doc_ID', 'Rank','Score'))
        # print newRow
        newDF = newDF.append(newRow,ignore_index=True)
        # newDF = pd.concat([newDF,newRow],)
        # print from1
        print "XXX"
    # print newDF

    #sort newDF
    newDF.sort(columns=['Score'],inplace=True,ascending=False)
    for i in range(0,len(newDF)):
        newDF.iloc[i]['Rank'] = i+1
    # print newDF

    return newDF




# print(getTopKDocsId(rankings1.loc[rankings1['Query_ID'] == 1],rankings2.loc[rankings2['Query_ID'] == 1],10))
# for rowNumber in range(0,len(rankings1)):
#     print (rankings1.iloc[rowNumber]['Doc_ID'])


def aggregate(titleTSV, textTSV, k, outputFile):
    titleDF = pd.read_csv(titleTSV, sep='\t')
    rankingsTitle_multiplier = 1
    textDF = pd.read_csv(textTSV, sep='\t')
    rankingsText_multiplier = 2

    query_id_set = set()
    for row in titleDF.iterrows():
        query_id_set.add(row[1]['Query_ID'])

    newDF = pd.DataFrame(columns=('Query_ID', 'Doc_ID', 'Rank', 'Score'))
    for query_id in query_id_set:
        topKDocs = getTopKDocsId(titleDF.loc[titleDF['Query_ID'] == query_id],
                                 textDF.loc[textDF['Query_ID'] == query_id], k)
        queryDF = combineTwoRankings(query_id, titleDF.loc[titleDF['Query_ID'] == query_id], 2,
                                     textDF.loc[textDF['Query_ID'] == query_id], 1, topKDocs)
        # print queryDF
        newDF = newDF.append(queryDF)

    newDF = newDF.reset_index()
    newDF.drop('index', axis=1, inplace=True)

    newDF.to_csv(outputFile, index=False, sep='\t')

aggregate("files/output_hw_EnglishStemmer_stopwords_BM25_title.tsv","files/output_hw_EnglishStemmer_stopwords_BM25_text.tsv",10,"EnglishStemmer_stopwords_BM25_ag.tsv")
