import pandas as pd
import math
import numpy

def buildQueryIdToDocIdDict(dataFrame, queryIdIndex, docIdIndex ):
    queryIDToDocsIdDict = {}
    for row in dataFrame.iterrows():
        queryID = row[1][queryIdIndex]
        docID = row[1][docIdIndex]

        if queryID in queryIDToDocsIdDict:
            docArray = queryIDToDocsIdDict[queryID]
            docArray.append(docID)
        else:
            docArray = []
            docArray.append(docID)
            queryIDToDocsIdDict[queryID] = docArray
    return  queryIDToDocsIdDict


def calculateRelevance(docID, groundTruthDocsArr):
    if docID in groundTruthDocsArr:
        return 1
    else:
        return 0

def calculateRPrecision(queryId, r, given_groundTruthDataQToD_Dict, given_resultsDataQToD_Dict):
    resultsDocsArr = given_resultsDataQToD_Dict[queryId]
    groundTruthDocsArr = given_groundTruthDataQToD_Dict[queryId]
    sumRelv = 0.0
    for rank in range(0, r):
        if rank > len(resultsDocsArr):
            break
        sumRelv += (calculateRelevance(resultsDocsArr[rank - 1], groundTruthDocsArr))

    return sumRelv / min(r,len(resultsDocsArr))


def calculateAverageRPrecision(toTestFile, groundTruthFile, k):
    groundTruthDataFrame = pd.read_csv(groundTruthFile, sep='\t')
    resultsDataFame = pd.read_csv(toTestFile, sep='\t')

    groundTruthDataQToD_Dict = buildQueryIdToDocIdDict(groundTruthDataFrame, 0, 1)
    resultsDataQToD_Dict = buildQueryIdToDocIdDict(resultsDataFame, 0, 1)

    rprecisionArr = []
    for queryID in resultsDataQToD_Dict.keys():
        rprecision = calculateRPrecision(queryID, k, groundTruthDataQToD_Dict, resultsDataQToD_Dict)
        rprecisionArr.append(rprecision)

    return numpy.average(rprecisionArr)
