import nMDCG
import AverageRPrecision
print  nMDCG.calculateAverageNMDCG("Count_Default.tsv","cran_Ground_Truth.tsv",k=10)

groundTruthFile = "cran_Ground_Truth.tsv"
filesToTest = ["files/output_hw_nostemmer_TfIdf.tsv",
               "files/output_hw_nostemmer_countscorer.tsv",
               "files/output_hw_EnglishStemmer_TfIdf.tsv",
               "files/output_hw_EnglishStemmer_stopwords_TfIdf.tsv",
               "files/output_hw_EnglishStemmer_stopwords_countscorer.tsv",
               "files/output_hw_EnglishStemmer_stopwords_BM25.tsv",
               "files/output_hw_EnglishStemmer_stopwords_BM25_title.tsv",
               "files/output_hw_EnglishStemmer_stopwords_BM25_text.tsv",
               "EnglishStemmer_stopwords_BM25_ag.tsv",
               "files/output_hw_EnglishStemmer_countscorer.tsv",
               "files/output_hw_EnglishStemmer_BM25.tsv"]
filesToTest_names = ["nostemmer_TfIdf",
               "nostemmer_countscorer",
               "EnglishStemmer_TfIdf",
               "EnglishStemmer_stopwords_TfIdf",
               "EnglishStemmer_stopwords_countscorer",
               "EnglishStemmer_stopwords_BM25",
               "EnglishStemmer_stopwords_BM25_title",
               "EnglishStemmer_stopwords_BM25_text",
                     "EnglishStemmer_stopwords_BM25_aggregated",
                     "EnglishStemmer_countscorer",
               "EnglishStemmer_BM25"]
print len(filesToTest)
results = [["file","title","nMDCG k=1","nMDCG k=3","nMDCG k=5","nMDCG k=10","Average R-Precision k=11"]]

for i in range(0,len(filesToTest)):
    aFile = filesToTest[i]
    fileTitle = filesToTest_names[i]

    row = []
    row.append(aFile)
    row.append(fileTitle)
    #R-Precission
    # row.append()

    #nMDCG
    row.append(nMDCG.calculateAverageNMDCG(aFile,groundTruthFile=groundTruthFile,k=1))
    row.append(nMDCG.calculateAverageNMDCG(aFile,groundTruthFile=groundTruthFile,k=3))
    row.append(nMDCG.calculateAverageNMDCG(aFile,groundTruthFile=groundTruthFile,k=5))
    row.append(nMDCG.calculateAverageNMDCG(aFile,groundTruthFile=groundTruthFile,k=10))
    row.append(AverageRPrecision.calculateAverageRPrecision(aFile,groundTruthFile=groundTruthFile,k=11))
    results.append(row)
    print row

import csv

with open("evalution_results.csv", "wb") as f:
    writer = csv.writer(f)
    writer.writerows(results)